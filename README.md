# Whoami
- Ingeniero Informático
- Pentester
- Scripter en Python y Bash
- Programador en Go

# Recursos
- https://vay3t.org
- https://blog.vay3t.org

# Proyectos/Herramientas activos

## Golang
- https://gitlab.com/vay3t/tiny-go-curl
- https://gitlab.com/vay3t/go-portscan
- https://gitlab.com/vay3t/go-pingport

## Scripts
- https://gitlab.com/vay3t/hax0rpi
- https://gitlab.com/vay3t/hashcrack-telegram-bot

# Presentaciones hechas por mi
Con <3 para la comunidad

## Proyectos

* Hashcrack Telegram Bot - Bsides Chile
    - Promo: https://www.youtube.com/watch?v=f4HZyFTkldc
    - Proyecto: https://gitlab.com/vay3t/hashcrack-telegram-bot
    - Video: https://youtu.be/YwhioxWlUWo?t=24918
    - Presentación: https://gitlab.com/vay3t/Presentaciones/-/blob/main/BsidesCL/hashcracktelegrambot.pdf

## Serie de Bash para pentesting

* Bash Scripting by Vay3t - L4tin-HTB
    * Video: https://www.youtube.com/watch?v=IT4GsN2IyGY
* Recon/pentesting con Bash - Hackmeeting BO
    * Video: https://youtu.be/lqSWOakmnqA?t=12682
    * Presentación: https://gitlab.com/vay3t/Presentaciones/-/blob/main/HackmeetingBO/BashHackmeetingBO.pdf

### Entradas de blog

* Creando un notificador en Telegram con Bash
   * https://vay3t.medium.com/creando-un-notificador-en-telegram-con-bash-b842490610

